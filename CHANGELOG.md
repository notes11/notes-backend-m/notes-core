# notes-core

## 0.0.6 (2021-05-17)
Added `<relativePath/> <!-- lookup parent from repository -->` in [pom.xml](pom.xml).

## 0.0.5 (2021-05-16)
CI correction in stage `gitlab-release`

## 0.0.4 (2021-05-16)
Further corrections for CI.

## 0.0.3 (2021-05-16)
CI correction

## 0.0.2 (2021-05-15)
Added hamcrest-all to pom.xml in dependencyManagement

## 0.0.1 (2021-05-15)
Initial release