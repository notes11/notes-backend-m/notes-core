package pl.mm.notes.core.model.dto;

import org.testng.annotations.Test;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.UUID;

import static org.testng.Assert.assertEquals;

public class RoleDtoTest {

    @Test
    public void testRoleDtoBuilder() {
        RoleDto role = RoleDto.builder()
                .uuid(UUID.randomUUID())
                .createdBy("createdBy")
                .createdTimestamp(Timestamp.from(Instant.now()))
                .modifiedBy("modifiedBy")
                .modificationTimestamp(Timestamp.from(Instant.now()))
                .roleName("roleName")
                .build();

        assertEquals(role.getUuid().getClass(), UUID.class);
        assertEquals(role.getCreatedBy(), "createdBy");
        assertEquals(role.getCreatedTimestamp().getClass(), Timestamp.class);
        assertEquals(role.getModifiedBy(), "modifiedBy");
        assertEquals(role.getModificationTimestamp().getClass(), Timestamp.class);
        assertEquals(role.getRoleName(), "roleName");
    }

}