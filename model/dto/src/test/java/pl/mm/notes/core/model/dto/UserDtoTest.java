package pl.mm.notes.core.model.dto;

import org.testng.annotations.Test;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.UUID;

import static org.testng.Assert.*;

public class UserDtoTest {

    @Test
    public void testUserDtoBuilder() {
        UserDto user = UserDto.builder()
                .uuid(UUID.randomUUID())
                .createdBy("createdBy")
                .createdTimestamp(Timestamp.from(Instant.now()))
                .modifiedBy("modifiedBy")
                .modificationTimestamp(Timestamp.from(Instant.now()))
                .userName("userName")
                .password("password")
                .firstName("firstName")
                .lastName("lastName")
                .addressEmail("addressEmail")
                .isActive(false)
                .build();

        assertEquals(user.getUuid().getClass(), UUID.class);
        assertEquals(user.getCreatedBy(), "createdBy");
        assertEquals(user.getCreatedTimestamp().getClass(), Timestamp.class);
        assertEquals(user.getModifiedBy(), "modifiedBy");
        assertEquals(user.getModificationTimestamp().getClass(), Timestamp.class);
        assertEquals(user.getUserName(), "userName");
        assertEquals(user.getPassword(), "password");
        assertEquals(user.getFirstName(), "firstName");
        assertEquals(user.getLastName(), "lastName");
        assertEquals(user.getAddressEmail(), "addressEmail");
        assertFalse(user.isActive());
        assertNull(user.getRoles());
    }

}