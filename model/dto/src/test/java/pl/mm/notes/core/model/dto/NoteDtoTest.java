package pl.mm.notes.core.model.dto;

import org.testng.annotations.Test;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.UUID;

import static org.testng.Assert.assertEquals;

public class NoteDtoTest {

    @Test
    public void testNoteDtoBuilder() {
        NoteDto note = NoteDto.builder()
                .uuid(UUID.randomUUID())
                .createdBy("createdBy")
                .createdTimestamp(Timestamp.from(Instant.now()))
                .modifiedBy("modifiedBy")
                .modificationTimestamp(Timestamp.from(Instant.now()))
                .title("title")
                .note("note")
                .build();

        assertEquals(note.getUuid().getClass(), UUID.class);
        assertEquals(note.getCreatedBy(), "createdBy");
        assertEquals(note.getCreatedTimestamp().getClass(), Timestamp.class);
        assertEquals(note.getModifiedBy(), "modifiedBy");
        assertEquals(note.getModificationTimestamp().getClass(), Timestamp.class);
        assertEquals(note.getTitle(), "title");
        assertEquals(note.getNote(), "note");
    }

}