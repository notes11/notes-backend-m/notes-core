package pl.mm.notes.core.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.sql.Timestamp;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@Data
public abstract class BaseInfoDto {

    protected UUID uuid;

    protected String createdBy;

    protected Timestamp createdTimestamp;

    protected String modifiedBy;

    protected Timestamp modificationTimestamp;

}
