package pl.mm.notes.core.model.db;

import org.testng.annotations.Test;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.UUID;

import static org.testng.Assert.assertEquals;

public class RoleTest {

    @Test
    public void testRoleBuilder() {
        Role role = Role.builder()
                .id(1)
                .uuid(UUID.randomUUID())
                .version(1L)
                .createdBy("createdBy")
                .createdTimestamp(Timestamp.from(Instant.now()))
                .modifiedBy("modifiedBy")
                .modificationTimestamp(Timestamp.from(Instant.now()))
                .entryRelatedToTable("entryRelatedToTable")
                .roleName("roleName")
                .build();

        assertEquals(role.getId(), 1);
        assertEquals(role.getUuid().getClass(), UUID.class);
        assertEquals(role.getVersion(), Long.valueOf(1));
        assertEquals(role.getCreatedBy(), "createdBy");
        assertEquals(role.getCreatedTimestamp().getClass(), Timestamp.class);
        assertEquals(role.getModifiedBy(), "modifiedBy");
        assertEquals(role.getModificationTimestamp().getClass(), Timestamp.class);
        assertEquals(role.getEntryRelatedToTable(), "entryRelatedToTable");
        assertEquals(role.getRoleName(), "roleName");
    }

}