package pl.mm.notes.core.converter;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.UUID;

public class UUIDStringConverterTest {

    @Test
    public void testConvertToDatabaseColumn() {
        UUID uuid = UUID.randomUUID();
        String expected = uuid.toString();
        UUIDStringConverter uuidStringConverter = new UUIDStringConverter();

        String actual = uuidStringConverter.convertToDatabaseColumn(uuid);

        Assert.assertEquals(actual, expected);
    }

    @Test
    public void testConvertToEntityAttribute() {
        UUID expectedUuid = UUID.randomUUID();
        String inputString = expectedUuid.toString();
        UUIDStringConverter uuidStringConverter = new UUIDStringConverter();

        UUID actual = uuidStringConverter.convertToEntityAttribute(inputString);

        Assert.assertEquals(actual, expectedUuid);
    }

}