package pl.mm.notes.core.model.db;

import org.testng.annotations.Test;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.UUID;

import static org.testng.Assert.*;

public class NoteTest {

    @Test
    public void testNoteBuilder() {
        Note note = Note.builder()
                .id(1)
                .uuid(UUID.randomUUID())
                .version(1L)
                .createdBy("createdBy")
                .createdTimestamp(Timestamp.from(Instant.now()))
                .modifiedBy("modifiedBy")
                .modificationTimestamp(Timestamp.from(Instant.now()))
                .entryRelatedToTable("entryRelatedToTable")
                .title("title")
                .note("note")
                .owner(new User())
                .build();

        assertEquals(note.getId(), 1);
        assertEquals(note.getUuid().getClass(), UUID.class);
        assertEquals(note.getVersion(), Long.valueOf(1));
        assertEquals(note.getCreatedBy(), "createdBy");
        assertEquals(note.getCreatedTimestamp().getClass(), Timestamp.class);
        assertEquals(note.getModifiedBy(), "modifiedBy");
        assertEquals(note.getModificationTimestamp().getClass(), Timestamp.class);
        assertEquals(note.getEntryRelatedToTable(), "entryRelatedToTable");
        assertEquals(note.getTitle(), "title");
        assertEquals(note.getNote(), "note");
        assertNotNull(note.getOwner());
    }

}