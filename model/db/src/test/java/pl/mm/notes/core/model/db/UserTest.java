package pl.mm.notes.core.model.db;

import org.testng.annotations.Test;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.UUID;

import static org.testng.Assert.*;

public class UserTest {

    @Test
    public void testUserBuilder() {
        User user = User.builder()
                .id(1)
                .uuid(UUID.randomUUID())
                .version(1L)
                .createdBy("createdBy")
                .createdTimestamp(Timestamp.from(Instant.now()))
                .modifiedBy("modifiedBy")
                .modificationTimestamp(Timestamp.from(Instant.now()))
                .entryRelatedToTable("entryRelatedToTable")
                .userName("userName")
                .password("password")
                .firstName("firstName")
                .lastName("lastName")
                .addressEmail("addressEmail")
                .isActive(false)
                .build();

        assertEquals(user.getId(), 1);
        assertEquals(user.getUuid().getClass(), UUID.class);
        assertEquals(user.getVersion(), Long.valueOf(1));
        assertEquals(user.getCreatedBy(), "createdBy");
        assertEquals(user.getCreatedTimestamp().getClass(), Timestamp.class);
        assertEquals(user.getModifiedBy(), "modifiedBy");
        assertEquals(user.getModificationTimestamp().getClass(), Timestamp.class);
        assertEquals(user.getEntryRelatedToTable(), "entryRelatedToTable");
        assertEquals(user.getUserName(), "userName");
        assertEquals(user.getPassword(), "password");
        assertEquals(user.getFirstName(), "firstName");
        assertEquals(user.getLastName(), "lastName");
        assertEquals(user.getAddressEmail(), "addressEmail");
        assertFalse(user.isActive());
        assertNull(user.getRoles());
        assertNull(user.getNotes());
    }

}