package pl.mm.notes.core.model.db;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import pl.mm.notes.core.converter.UUIDStringConverter;
import java.sql.Timestamp;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@Data
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "base_info")
public abstract class BaseInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    @Column(name = "Id", unique = true, nullable = false, updatable = false)
    protected long id;

    @Convert(converter = UUIDStringConverter.class)
    @Column(name = "Uuid", unique = true, nullable = false, updatable = false, length = 36)
    protected UUID uuid;

    @Version
    private Long version;

    @Column(name = "CreatedBy", nullable = false, updatable = false, length = 50)
    protected String createdBy;

    @CreationTimestamp
    @Column(name = "CreatedTimestamp", nullable = false, updatable = false)
    protected Timestamp createdTimestamp;

    @Column(name = "ModifiedBy", length = 50)
    protected String modifiedBy;

    @UpdateTimestamp
    @Column(name = "ModificationTimestamp")
    protected Timestamp modificationTimestamp;

    @Column(name = "EntryRelatedToTable", nullable = false, length = 50)
    protected String entryRelatedToTable;

}
