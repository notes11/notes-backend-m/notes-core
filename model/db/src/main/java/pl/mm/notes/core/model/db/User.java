package pl.mm.notes.core.model.db;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import java.util.Collection;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@Entity
@Table(name = "user")
public class User extends BaseInfo {

    @Length(min = 5, max = 50, message = "Username need contains at least 5 characters and at most 50 characters.")
    @NotBlank(message = "User name cannot be empty.")
    @Column(name = "UserName", unique = true, nullable = false, length = 50)
    private String userName;

    @Length(min = 5, max = 80, message = "Password need contains at least 5 characters and at most 80 characters.")
    @NotBlank(message = "Password cannot be empty.")
    @Column(name = "Password", nullable = false, length = 80)
    private String password;

    @Length(max = 50, message = "First name can contains at most 50 characters.")
    @Column(name = "FirstName", nullable = false, length = 50)
    private String firstName;

    @Length(max = 50, message = "Last name can contains at most 50 characters.")
    @Column(name = "LastName", nullable = false, length = 50)
    private String lastName;

    @Length(max = 100, message = "Email address need contains at most 100 characters.")
    @Pattern(regexp = "(^[A-z0-9]+)(@)([A-z]+)(\\.)([A-z]+$)", message = "Email address has incorrect format.")
    @NotBlank(message = "Email address cannot be empty.")
    @Column(name = "AddressEmail", nullable = false, length = 100)
    private String addressEmail;

    @Enumerated(EnumType.STRING)
    @Column(name = "IsActive", nullable = false)
    private boolean isActive;

    @NotEmpty
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "user_roles",
            joinColumns = @JoinColumn(name = "UserId"),
            inverseJoinColumns = @JoinColumn(name = "RoleId"))
    private Collection<Role> roles;

    @OneToMany(mappedBy = "owner", fetch = FetchType.LAZY)
    private Collection<Note> notes;


}
